# ToDo List API

This project demonstrates the technical capabilities in software development.

## Tech Stack
- Node/Express
- Mongoose 
- MongoDB
- JWT Authentication
- Mocha/SuperTest

## Running Instructions

We strongly recommend using OSX or Linux operation system. This project uses docker and docker-compose, which can be a bit quirky on Windows machines.

1. Install Docker (if you don't already have it installed) [An guide here.](https://docs.docker.com/install/ "a guide here.")

2. Clone the project repository with this command.
```
git clone git@gitlab.com:alejandro.sanchez1992/todo-list.git
```
*Note:* Make sure you add the ssh key of your machine to the repository in the cloud or clone through HTTP.

Then go to the project directory: `cd todo-api`

3. Run `docker-compose up` in the console. This will create the MongoDB and Node/Express server docker containers. The MongoDB just uses the MongoDB official image, while the API server uses the Dockerfile to build the container. **This could take a bit of time the first again.**  

You should see two running containers. 

4. Finally, in the browser go to the following direction: `127.0.0.1:3000` You should see a welcome message. 

*Note:* All available endpoints of the API are in the **"REST API Specification"** section.

## Running Test Suite

Before getting started, make sure of running the Docker containers as in the **"Running Instructions"**.

1. Attach a new bash session in the docker container Node/Express with the next command.

```
docker exec -it todo-api-express bash
```

2. Run test cases  

```
npm test 
```

You should review the test results.

Also you can run UI Test with [Postman](https://www.getpostman.com/). Download this [File](/src/tests/postman/todo-api.postman_collection.json) and import in Postman. 

## Database Migration Scripts

Run mongodump with docker from the system command line to make a database backup. **In the script command, replace the values between double quotes to real values.**

```
docker exec todo-api-mongodb sh -c 'mongodump --username "<USERNAME>" --password "<PASSWORD>" --archive' > db.dump
```

*Note: make sure to run the Docker containers.*

Now, use mongorestore to restore the database.

```
docker exec -i todo-api-mongodb sh -c 'mongorestore --username "<USERNAME>" --password "<PASSWORD>" --archive' < db.dump
```

## REST API Specification

| Route | Method  | Payload | Return | Description | 
|--|--|--|--|--|
| /login | POST | { email: string, password: string } | Users Model | SignIn users | 
| /logout | POST | { xxx-access-token } |  Message | Logout users | 
| /sign-up | POST | { fullname: string, email: string, password: string } | Users Model | Create users |
| /tasks | GET | { xxx-access-token } | Tasks Model | Get tasks |
| /tasks | POST | { xxx-access-token } {description: string, date: datetime, category_id: number} | Tasks Model | Create tasks |
| /tasks/{id} | PUT | { xxx-access-token } { description: string, date: datetime, category_id: number } | Message | Edit tasks |
| /tasks/{id} | GET | { xxx-access-token } | Tasks Model | Get task By Id |
| /tasks/{id} | DELETE |{ xxx-access-token } | Message | Delete tasks |
| /tasks/{id}/completed | POST | { xxx-access-token } | Tasks Model |  Mark the tasks as completed |
| /categories | GET | { xxx-access-token }| Categories Model |  Get categories |
| /categories | POST | { xxx-access-token } {category_name: string }| Categories Model |  Create categories |

## Database Models

    Users:
    {
        id: Number,
        fullname: String,
        email: String,
        password: String
    }
 
    Tasks:
    {
        id: Number
        description: String,
        date: Datetime,
        status: String,
        category: Categories,
        user: Users
    }

    Categories:
    {
        id: Number,
        name: String,
        user: Users
    }


