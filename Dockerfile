# Set the base image
FROM node:latest

# Metadata information
LABEL "co.todo.appNode"="Alejandro Sanchez"
LABEL maintainer="alejosanosp@gmail.com"
LABEL version="1.0"


# Create working directory
RUN mkdir -p /app

# The working directory is established
WORKDIR /app

# Install the existing packages in the package.json
COPY package.json .
RUN npm install

# Installing Nodemon Globally
# When changes are made, restart the server
RUN npm install nodemon -g --no-optional

# Copy the Application
COPY . .

# Expose the application in the port 3000
EXPOSE 3000

# Start the application when you start the container
CMD npm start
