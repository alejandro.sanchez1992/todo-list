import bcrypt from 'bcrypt';
import User from '../../models/user';
import request from 'supertest';
import app from '../../app';

const defaultUser = {
	fullname: 'Test Name User',
	email: 'user@test.com',
	password: 'pw2019'
};

const createUser = async () => {
    const user = new User(defaultUser);
    await user.save();
    return user;
};
 
export const getDefaultUserTest = async () => {
    let users = await User.find({ "email" : defaultUser.email });
    if (users.length === 0) {
        users = await createUser();
    } else {
        users = users[0];
    }
    return users;
};


export const loginWithDefaultUser = async () => {
    await getDefaultUserTest();
    return request(app)
            .post('/auth')
            .send({ 
                email: defaultUser.email, 
                password: defaultUser.password
            })
            .expect(200); 
}