import Task from '../../models/task';

let defaultTask = {
    description: 'Task Test',
    userId: ''
};

const createTask = async () => {
    const task = new Task(defaultTask);
    task.user = defaultTask.userId;
    await task.save();
    return task;
};
 
export const getDefaultTaskTest = async (userId) => {
    let tasks = await Task.find({ "user" : userId});
    defaultTask.userId = userId;
    if (tasks.length === 0) {
        tasks = await createTask();
    } else {
        tasks = tasks[0];
    }
    return tasks;
};