import bcrypt from 'bcrypt';
import Category from '../../models/category';

let defaultCategory = {
	name: 'Category Test',
    userId: ''
};

const createCategory = async (userId) => {
    const category = new Category(defaultCategory);
    category.user = defaultCategory.userId;
    await category.save();
    return category;
};
 
export const getDefaultCategoryTest = async (userId) => {
    let categories = await Category.find({ "name" : defaultCategory.name});
    defaultCategory.userId = userId;
    if (categories.length === 0) {
        categories = await createCategory(userId);
    } else {
        categories = categories[0];
    }
    return categories;
};
