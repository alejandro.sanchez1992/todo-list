import Chance from 'chance';
import request from 'supertest';
import app from '../app';


/**
 * Testing API welcome message 
 */
describe('GET /', function () {
    it('respond with json containing a welcome message', function (done) {
        request(app)
            .get('/')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

describe('POST /users', function() {
	
	let chance = new Chance();
	/**
	 * Testing create a user
	*/
	it('respond with json cotaining user created', function(done) {
		console.log("EMAIL:",chance.email());
		request(app)
		  .post('/users')
		  .send({fullname: chance.name(),email: chance.email() ,password: 'pw2019'})
		  .set('Accept', 'application/json')
		  .expect('Content-Type', /json/)
		  .expect(201)
		  .end(function(err, res) {
		    if (err) return done(err);
		    done();
		  });
	});

	/**
	 * Testing create a exist user
	*/
	it('respond with an error message', function(done) {
		request(app)
		  .post('/users')
		  .send({fullname: 'Name',email: 'name@test.com',password: 'pw2019'})
		  .set('Accept', 'application/json')
		  .expect('Content-Type', /json/)
		  .expect(400)
		  .end(function(err, res) {
		    if (err) return done(err);
		    done();
		  });
	});
});

