import { loginWithDefaultUser } from './fixtures/authFixture';
import { getDefaultCategoryTest } from './fixtures/categoryFixture';
import { getDefaultTaskTest } from './fixtures/taskFixture';
import request from 'supertest';
import app from '../app';

describe('Testing Tasks', () => {
	
	let userToken = '';
	let user = null;

  	before( async () => {
  		let response = await loginWithDefaultUser();	
  		userToken = String(response.body.token);
  		user = response.body.user;
  	});
  	
	describe('GET /tasks', function () {

		/**
		 * Testing get all tasks 
		*/	
	    it('Should respond with json containing a list of all task', function (done) {
	        request(app)
	            .get('/tasks')
	            .set('Accept', 'application/json')
	            .set('x-access-token', userToken)
	            .expect('Content-Type', /json/)
	            .expect(200, done);
	    });

	    /**
		 * Testing get all tasks without token
		*/
	    it('Should respond with an error message: No token provided.', function (done) {
	        request(app)
	            .get('/tasks')
	            .set('Accept', 'application/json')
	            .expect('Content-Type', /json/)
	            .expect(401, { 
	            	success: false,
					message: "No token provided."
				} ,done);
	    });

	    /**
		 * Testing get all tasks with incorrect token
		*/	
	    it('Should respond with an error message: Failed to authenticate token.', function (done) {
	        request(app)
	            .get('/tasks')
	            .set('Accept', 'application/json')
	            .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6')
	            .expect('Content-Type', /json/)
	            .expect(401, { 
	            	success: false,
					message: "Failed to authenticate token."
				} ,done);
	    });
	});


	describe('POST /tasks', function () {
		
		let categoryId = null;

		before( async () => {
	  		let category = await getDefaultCategoryTest(user._id);
	  		categoryId = String(category._id);
	  	});

		/**
		 * Testing create task
		*/	
	    it('Should respond with json containing the created task', function (done) {

	        request(app)
	            .post('/tasks')
	            .send({
	            	description: 'Make the new generate of humans', category: categoryId
	            })
	            .set('Accept', 'application/json')
	            .set('x-access-token', userToken)
	            .expect('Content-Type', /json/)
	            .expect(201, done);
	    });

	});

	describe('PUT /tasks', function () {
		
		let taskId = null;

		before( async () => {
	  		let task = await getDefaultTaskTest(user._id);
	  		taskId = String(task._id);
	  	});

		/**
		 * Testing update task
		*/	
	    it('Should respond with json containing an messsage success', function (done) {
	        request(app)
	            .put(`/tasks/${taskId}`)
	            .send({
	            	description: 'Make the new generate of humans [Update]'
	            })
	            .set('Accept', 'application/json')
	            .set('x-access-token', userToken)
	            .expect('Content-Type', /json/)
	            .expect(200, done);
	    });

	});

	describe('DELETE /tasks', function () {
		
		let taskId = null;

		before( async () => {
	  		let task = await getDefaultTaskTest(user._id);
	  		taskId = String(task._id);
	  	});

		/**
		 * Testing delete task
		*/	
	    it('Should respond with json containing an messsage success', function (done) {
	        request(app)
	            .delete(`/tasks/${taskId}`)
	            .set('Accept', 'application/json')
	            .set('x-access-token', userToken)
	            .expect('Content-Type', /json/)
	            .expect(200, done);
	    });

	});


	describe('GET /tasks', function () {

		let taskId = null;

		before( async () => {
	  		let task = await getDefaultTaskTest(user._id);
	  		taskId = String(task._id);
	  	});

		/**
		 * Testing get task by Id 
		*/	
	    it('Should respond with json containing a task by Id', function (done) {
	        request(app)
	            .get(`/tasks/${taskId}`)
	            .set('Accept', 'application/json')
	            .set('x-access-token', userToken)
	            .expect('Content-Type', /json/)
	            .expect(200, done);
	    });

    });

});

