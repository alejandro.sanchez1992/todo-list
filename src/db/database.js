import Mongoose from 'mongoose';
import config from 'config';

const stringConnection = `mongodb://${config.database.host}:${config.database.port}/${config.database.name}?authSource=admin`

Mongoose.connect(stringConnection,{
	auth: {
	    user: config.database.user,
	    password: config.database.password
  	},
  	useNewUrlParser: true 
});

const db = Mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error.'));
db.once('open', function callback() {
  console.log("Connection with database succeeded.");
});

exports.db = db;
