import express from 'express';
import bodyParser from 'body-parser';
import mongoose from'mongoose';
import morgan from 'morgan';
import jwt from'jsonwebtoken';

import User from './models/user';
import Task from './models/task';
import Category from './models/category';


import config from 'config';
import db from './db/database';
import routes from './routes';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/', routes);

if(!module.parent)
	app.listen(config.server.port,config.server.host);

console.log('Todo List Api listening on: ', `${config.server.host}:${config.server.port}`);

module.exports = app;
