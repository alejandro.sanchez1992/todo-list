import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const TaskSchema = new Schema({
  description: {
    type: String,
    required: true
  },
  status: {
    type: Boolean,
    default: false
  },
  due_date:{
  	type : Date, 
  	default: Date.now
  },
  user: { 
  	type: Schema.Types.ObjectId, 
  	ref: 'User',
  	required: true 
  },
  category: {
    type: Schema.Types.ObjectId, 
    ref: 'Category',
  }
});

module.exports = mongoose.model('Task', TaskSchema);
