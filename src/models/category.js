import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  user: { 
  	type: Schema.Types.ObjectId, 
  	ref: 'User',
  	required: true 
  },
});

module.exports = mongoose.model('Category', CategorySchema);
