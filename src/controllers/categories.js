import mongoose from 'mongoose';
import response from '../helpers/response';
import request from '../helpers/request';

const Category = mongoose.model('Category');

exports.list = function(req, res) {
  Category.find({user: req.currentUser}, function(err, categories) {
    res.json(categories);
  });
};

exports.create = function(req, res) {
  const category = new Category(req.body);
  category.user = req.currentUser;
  category.save(function(err, category) {
    response.sendCreated(res, category);
  });
};

