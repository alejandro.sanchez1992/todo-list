import mongoose from 'mongoose';
import response from '../helpers/response';
import request from '../helpers/request';
import jwt from 'jsonwebtoken';
import config from 'config';

const User = mongoose.model('User');

exports.create = function(req, res) {
  const newUser = new User(req.body);
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return response.sendBadRequest(res, err);
    let token = jwt.sign(user.getTokenData(), config.key.privateKey, {
        expiresIn: config.key.tokenExpireInMinutes
    });
    response.sendCreated(res, {user: user, token: token});
  });
};


exports.read = function(req, res) {
  console.log(req.params.id);	
  User.findById(req.params.id, function(err, user) {
    if (user == null) return response.sendNotFound(res);
    res.json(user);
  });
};


