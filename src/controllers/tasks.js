import mongoose from 'mongoose';
import response from '../helpers/response';
import request from '../helpers/request';

const Task = mongoose.model('Task');

/*
  *
  * Get all Tasks  
*/
module.exports.list = async (req, res) => {
    const tasks = await Task.find({user: req.currentUser})
      .populate('category', 'name _id')
      .select('due_date description status category');
    res.json(tasks);
}

/*
  *
  * Create Tasks  
*/
module.exports.create = async (req, res) => {
  const task = new Task(req.body);
  task.user = req.currentUser;
  await task.save();
  response.sendCreated(res, task);
};


/*
  *
  * Read Tasks  
*/
module.exports.read = async (req, res) => {
  const task = await Task.findById(req.params.id);
  if(task == null)return response.sendNotFound(res);
  res.json(task);
};


/*
  *
  * Update Tasks  
*/
module.exports.update = async (req, res) => {
  const task = await Task.findByIdAndUpdate(req.params.id, req.body);
  if(task == null)return response.sendNotFound(res);
  res.json({ message: 'Task successfully Updated'});
};

/*
  *
  * Delete Tasks  
*/
module.exports.delete = async (req, res) => {
  const task = await Task.findByIdAndRemove(req.params.id);
  if(task == null)return response.sendNotFound(res);
  res.json({ message: 'Task successfully deleted' });
};



/*
  *
  * Update tasks like completed  
*/
module.exports.completed = async (req, res) => {
  let { status } = req.body;
  const task = await Task.findByIdAndUpdate(req.params.id, { status });
  if(task == null)return response.sendNotFound(res);
  res.json({ message: 'Task completed'});
};

/*
  *
  * Get tasks filtering by category
*/
module.exports.filterByCategory = async (req, res) => {
  console.log("Filter by category");
  const tasks = await Task.find()
      .where('category').equals(req.params.id)
      .populate('category', 'name _id')
      .select('due_date description status category');
    res.json(tasks);
};




