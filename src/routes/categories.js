import express from 'express';

import categoriesController from '../controllers/categories';
import authController from '../controllers/auth';

const routes  = express.Router();

routes.use(authController.verifyToken);

routes.route('/').get(categoriesController.list).post(categoriesController.create);

module.exports = routes;