import express from 'express';

import usersController from '../controllers/users';
import authController from '../controllers/auth';

const routes  = express.Router();

routes.route('/').post(usersController.create);

routes.route('/:id').all(authController.verifyToken).get(usersController.read);

module.exports = routes;
