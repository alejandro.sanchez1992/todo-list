import express from 'express';

import auth from './auth';
import users from './users';
import response from '../helpers/response';
import tasks from './tasks';
import categories from './categories';

const routes  = express.Router();

routes.use(response.setHeadersForCORS);

routes.use('/auth', auth);
routes.use('/users', users);
routes.use('/tasks',tasks);
routes.use('/categories',categories);

routes.get('/test', (req, res) => {
	res.status(200).json({ message: 'Welcome to ToDo List API!!!' });	
});

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Welcome to ToDo List API!!!' });
});

routes.use(function(req, res) {
  response.sendNotFound(res);
});

module.exports = routes;
