import express from 'express';

import taskController from '../controllers/tasks';
import authController from '../controllers/auth';

const routes  = express.Router();

routes.use(authController.verifyToken);

routes.route('/').get(taskController.list).post(taskController.create);

routes.route('/:id').get(taskController.read).put(taskController.update).delete(taskController.delete);

routes.route('/:id/completed').put(taskController.completed);

routes.route('/category/:id').get(taskController.filterByCategory);

module.exports = routes;
