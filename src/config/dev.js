const DB_HOST = process.env.DB_HOST || 'db';

const SERVER_HOST = process.env.HOST || '0.0.0.0';
const SERVER_PORT = process.env.PORT || 3000;

module.exports = {
  server: {
    port: SERVER_PORT,
    host: SERVER_HOST
  },
  database: {
    name: "to-do-list",
    host: DB_HOST,
    port: "27017",
    user: "mdbuser",
    password: "QBpW2xfqULDx2qLuQN6nsDup4qqUzZP3h",
  },
  key: {
    privateKey: '37LvDSm4XvjYOh9Y',
    tokenExpireInMinutes: 8440
  }
};
